#!/usr/bin/env bash

main() {
    swagger-cli-bundle
}

swagger-cli-bundle(){
    create-empty-file
    append-admonishment
    append-bundled-specification
}

create-empty-file() {
    mkdir -p build
    rm -rf build/openapi.yaml
    touch build/openapi.yaml
}

append-admonishment() {
    cat << EOM > build/openapi.yaml
# DO NOT EDIT THIS FILE
# This file is generated by the build in the API project.
# Instead edit the specification in specification/ of the API project."
EOM
}

append-bundled-specification() {
    swagger-cli bundle --type yaml specification/index.yaml >> build/openapi.yaml
}

main
